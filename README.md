# JSDoc SuiteScript Plugin

Adds tag definitions for several SuiteScript-related tags. Includes a template
based on the JSDoc default template.

![Example of generated output that uses all tags](example.png)

## Installation

Install the npm package:

`npm i jsdoc-plugin-suitescript`

The tag definitions and default template are automatically copied to the local
`jsdoc` module folder via `npm`'s `postinstall` script.

Add the plugin to your JSDoc configuration:

```
{
    "plugins": ["plugins/suitescript"]
}
```

If you use a JSDoc template other than the default, you can copy the relevant
files from `node_modules/jsdoc/templates/default/tmpl/` to your own template
folder.

## `@governance`

### Synonyms

* `@gov`

### Syntax

`@governance <Number|String>`

`@gov <Number|String>`

### Overview

The `@governance` tag is used to document the NetSuite governance usage of a
function. The usage can be in the form of a Number or a text formula describing
the necessary calculation.

### Examples

In the following example, the documented function performs a single search,
which uses 10 governance units.

```
/**
 * Performs a transaction search
 *
 * @governance 10
 */
function doSearch() {
    return nlapiSearchRecord('transaction') || [];
}
```

In the following example, the documented function submits each custom record in
a given list, which uses 4 governance units per record.

```
/**
 * Submits each record in the given list
 *
 * @governance (4 * n), where n is the number of records in the given list
 */
function saveRecords(records) {
    if (!records.length) { return; }
    
    records.forEach(function (rec) { nlapiSubmitRecord(rec); });
}
```

## `@NApiVersion`

### Syntax

`@NApiVersion <version>`

`version` can be any of:

* 1.0
* 2.0
* 2.x
* 2.X

### Overview

The `@NApiVersion` tag is used to document the SuiteScript version used by a
script file or module. It can serve as a defense against future incompatible
versions of SuiteScript (versions 3.x and higher) attempting to load it.

### Examples

In the following example, a 2.0 Client Script module is documented.

```
/**
 * A sample Client script module
 *
 * @NApiVersion 2.x
 * @NModuleScope Public
 * @NScriptType ClientScript
 */
define(["N/search"], function (s) {

    var exports = {};
    // ...
```

## `@NModuleScope`

### Syntax

`@NModuleScope <scope>`

`scope` can be any of:

* SameAccount
* TargetAccount
* Public

### Overview

The `@NModuleScope` tag is used to document the access level of a SuiteScript
module.

* If the value is set to `SameAccount`, access to the module is limited to other
modules from the same bundle, and modules native to the same source account or
sandbox environment. Source code is *not hidden* at runtime.
* If the value is set to `TargetAccount`, access to the module is limited to
other modules from the same bundle, and modules native to the same source
account, target account, or sandbox environment. Source code *is hidden* at
runtime.
* If the value is set to `Public`, any script in the account can load and use
the module. Source code *is hidden* at runtime.

### Examples

In the following example, a 2.0 Client Script module is documented such that it
is only visible to modules native to the same account.

```
/**
 * A sample Client script module only visible to the same account or sandbox
 *
 * @NApiVersion 2.0
 * @NModuleScope SameAccount
 * @NScriptType ClientScript
 */
define(["N/search"], function (s) {

    var exports = {};
    // ...
```

## `@NScriptType`

### Syntax

`@NScriptType <scriptType>`

`scriptType` can be any of:

* BundleInstallationScript
* ClientScript
* MapReduceScript
* MassUpdateScript
* Portlet
* Restlet
* ScheduledScript
* Suitelet
* UserEventScript
* WorkflowActionScript

### Overview

The `@NScriptType` tag is used to document the type of Script record a
SuiteScript module represents.

### Examples

In the following example, a 2.0 Client Script module is documented.

```
/**
 * A sample Client script module only visible to the same account or sandbox
 *
 * @NApiVersion 2.X
 * @NModuleScope TargetAccount
 * @NScriptType ClientScript
 */
define(["N/search"], function (s) {

    var exports = {};
    // ...
```
